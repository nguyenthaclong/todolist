<?php

namespace TDL\TestApi;

use Faker\Factory as FakerFactory;
use ReflectionClass;
use ReflectionProperty;
use TDL\ActorUser\Usecases\BoundaryDataFactory;
use TDL\ActorUser\Usecases\BoundaryDataInterface;


/**
 * Class DataModelHelper
 * @package TDL\TestApi
 * Use to decouple data structure from tests
 */
class DataModelHelper {

    /**
     * @var BoundaryDataFactory
     */
    private $dataStructureFactory;

    /**
     * @var string
     */
    private $useCase;

    public function __construct(BoundaryDataFactory $dataFactory, string $useCase)
    {
        $this->dataStructureFactory = $dataFactory;
        $this->useCase = $useCase;
    }

    /**
     * @return BoundaryDataInterface
     */
    public function createFakeResponseData(): BoundaryDataInterface
    {
        $responseData = $this->dataStructureFactory->getInstanceResponseData($this->useCase);
        $structure    = $this->getPropertiesBoundaryData($responseData);
        return $this->generateFakeData($responseData, $structure);
    }

    /**
     * @return BoundaryDataInterface
     */
    public function createFakeRequestData(): BoundaryDataInterface
    {
        $requestData = $this->dataStructureFactory->getInstanceRequestData($this->useCase);
        $structure    = $this->getPropertiesBoundaryData($requestData);
        return $this->generateFakeData($requestData, $structure);
    }


    /**
     * @param BoundaryDataInterface $boundaryData
     * @param array $structure
     * @return BoundaryDataInterface
     */
    protected function generateFakeData(BoundaryDataInterface $boundaryData, array $structure)
    {
        $faker = FakerFactory::create();
        $dateFields = $boundaryData->getDateFields();
        foreach ($structure as $propName => $propOptions) {
            switch ($propOptions['type']) {
                case 'bool':
                    $boundaryData->$propName = $faker->boolean;
                    break;
                case 'int':
                    $boundaryData->$propName = $faker->numberBetween(0,2);
                    break;
                case 'string':
                    if (array_key_exists($propName, $dateFields)) {
                        $boundaryData->$propName = $faker->date($dateFields[$propName]);
                    } else {
                        $boundaryData->$propName = $faker->name;
                    }
                    break;
                default:
                    $boundaryData->$propName = $faker->name;
                    break;
            }
        }
        return $boundaryData;
    }

    /**
     * @param BoundaryDataInterface $boundaryData
     * @return array
     */
    protected function getPropertiesBoundaryData(BoundaryDataInterface $boundaryData)
    {
        try {
            $reflect = new ReflectionClass($boundaryData);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            $output = [];
            foreach ($props as $prop) {
                $name       = $prop->getName();
                $isOptional = !in_array($prop->getName(), $boundaryData->getMandatoryFields());
                $typeProp   = $this->getPropertyTypeFromDocComment($prop->getDocComment());
                $output[$name] = ['optional'=> $isOptional, 'type' => $typeProp];
            }
            return $output;
        } catch (\Exception $e) {
            return ['ReflexionError' => $e->getMessage()];
        }
    }

    /**
     * Parse doc comment to get type of property
     * @param string $docComment
     * @return string
     */
    protected function getPropertyTypeFromDocComment(string $docComment) {
        $defaultType = 'string';
        $matches = [];

        preg_match('/\*\s+@var\s+(\S+)\s+.*/m', $docComment, $matches);
        if (isset($matches[1]) && !empty($matches[1])) {
            return $matches[1];
        } else {
            return $defaultType;
        }
    }
}