<?php
namespace TDL\ActorUser\Usecases\MarkTaskDone\Boundaries;

use TDL\ActorUser\Usecases\BoundaryDataInterface;

/**
 * Interface RequesterInterface
 * Assure inverse dependency abstract link with CONTROLLERS/ADAPTERS LAYER
 * and inverse dependency abstract link USE CASE LAYER
 *
 * Particularity of this separation is :
 * -------------------------------------
 * It assure protection USE CASE layer from CONTROLLER layer changes
 * and also protect CONTROLLER layer from USE CASE changes
 * @package TDL\ActorUser\Usecases\CreateTask
 */
interface RequesterInterface {
    public function markTaskDone(BoundaryDataInterface $requestData) : BoundaryDataInterface;
}
