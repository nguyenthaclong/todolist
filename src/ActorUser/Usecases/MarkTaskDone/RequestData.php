<?php
namespace TDL\ActorUser\Usecases\MarkTaskDone;

use TDL\ActorUser\Usecases\BoundaryDataInterface;

final class RequestData implements BoundaryDataInterface {

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $date;

    /**
     * @var int
     */
    public $status;

    /**
     * @var bool
     */
    public $deleted;

    /** @var string */
    public $error;


    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->id) && empty($this->title) &&
            empty($this->label) && empty($this->date) &&
            empty($this->status) && empty($this->deleted) &&
            empty($this->deleted));

    }

    public function getMandatoryFields(): array
    {
        return ['id', 'title', 'status'];
    }

    public function getDateFields(): array
    {
        return ['date' => 'Y-m-d H:i'];
    }
}