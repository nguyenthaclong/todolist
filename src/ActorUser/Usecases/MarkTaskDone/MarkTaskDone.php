<?php


namespace TDL\ActorUser\Usecases\MarkTaskDone;


use TDL\Entities\EntityFactory;
use TDL\Entities\Task\Exception\TaskNotValidException;
use TDL\Entities\Task\Task;

use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\MarkTaskDone\Boundaries\RepositoryInterface;
use TDL\ActorUser\Usecases\MarkTaskDone\Boundaries\RequesterInterface;
use TDL\ActorUser\Usecases\MarkTaskDone\Exception\MarkTaskDoneRuntimeException;


final class MarkTaskDone implements RequesterInterface
{

    /** @var EntityFactory */
    private $entityFactory;

    /** @var RepositoryInterface */
    private $repository;

    /**
     * @param EntityFactory $entityFactory
     * @param RepositoryInterface $repository
     */
    public function __construct(EntityFactory $entityFactory,
                                RepositoryInterface $repository)
    {
        $this->entityFactory = $entityFactory;
        $this->repository    = $repository;
    }

    /**
     * Manage use case mark task done
     * @param BoundaryDataInterface $requestData
     * @return ResponseData
     * @throws MarkTaskDoneRuntimeException
     * @throws TaskNotValidException
     */
    public function markTaskDone(BoundaryDataInterface $requestData) : BoundaryDataInterface
    {
        # get input data
        /** @var RequestData $input */
        $input = $requestData;

        # use case scenario here
        /** @var Task $task */
        $task = $this->entityFactory->getInstanceTask();
        $taskCreated = $task->update($input->id, $input->title, $input->label, $input->date, intval($input->status));
        $taskCreated->markDone();

        # persist data
        /** @var Task $taskDone */
        $taskDone = $this->repository->persistTaskDone($taskCreated);
        if (false === $taskDone) {
            throw new MarkTaskDoneRuntimeException();
        }

        # prepare response data
        $output = new ResponseData();
        $output->id     = $taskDone->getId();
        $output->title  = $taskDone->getTitle();
        $output->label  = $taskDone->getLabel();
        $output->date   = $taskDone->getDate();
        $output->status = $taskDone->getStatus();
        return $output;
    }

}