<?php
namespace TDL\ActorUser\Usecases\MarkTaskDone\Presenters;

use TDL\ActorUser\Usecases\MarkTaskDone\RequestData;

/**
 * Interface MarkTaskDoneViewInterface
 * Assure inverse dependency abstract link with VIEWS (UI) LAYER
 * @package TDL\ActorUser\Presenters
 */
interface MarkTaskDoneViewInterface {

    public function showMarkTaskDoneInputForm() : RequestData;

    public function showMarkTaskDoneResponse(MarkTaskDoneViewData $viewData);

    public function showMarkTaskDoneInputDataInvalid();

    public function showMarkTaskDoneRunTimeError();
}