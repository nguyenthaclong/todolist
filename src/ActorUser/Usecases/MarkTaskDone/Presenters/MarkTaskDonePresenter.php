<?php

namespace TDL\ActorUser\Usecases\MarkTaskDone\Presenters;

use TDL\ActorUser\Controllers\Boundaries\MarkTaskDonePresenterInterface;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\MarkTaskDone\ResponseData;

class MarkTaskDonePresenter implements MarkTaskDonePresenterInterface
{

    /** @var MarkTaskDoneViewInterface */
    private $view;

    public function __construct(MarkTaskDoneViewInterface $view)
    {
        $this->view = $view;
    }

    public function presentMarkTaskDoneInputForm()
    {
        # print out
        $this->view->showMarkTaskDoneInputForm();
    }

    public function presentMarkTaskDone(BoundaryDataInterface $responseData)
    {
        # transform response data structure to a view data structure (in this case console view type)
        /** @var ResponseData $responseData */
        $viewDataStruct = new MarkTaskDoneViewData();
        $viewDataStruct->id      = $responseData->id;
        $viewDataStruct->title   = $responseData->title;
        $viewDataStruct->label   = $responseData->label;
        $viewDataStruct->status  = $responseData->status;
        $viewDataStruct->deleted = $responseData->deleted;

        # print out
        $this->view->showMarkTaskDoneResponse($viewDataStruct);
    }


    public function presentMarkTaskDoneRunTimeException()
    {
        # print out
        $this->view->showMarkTaskDoneRunTimeError();
    }

    public function presentTaskNotValidException()
    {
        # print out
        $this->view->showMarkTaskDoneInputDataInvalid();
    }
}