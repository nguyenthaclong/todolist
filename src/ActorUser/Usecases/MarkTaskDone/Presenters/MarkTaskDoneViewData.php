<?php

namespace TDL\ActorUser\Usecases\MarkTaskDone\Presenters;

class MarkTaskDoneViewData
{
    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $date;

    /** @var string */
    public $label;

    /** @var string */
    public $status;

    /** @var bool */
    public $deleted;

}