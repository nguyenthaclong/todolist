<?php


namespace TDL\ActorUser\Usecases;


use TDL\ActorUser\Usecases\CreateTask\RequestData as CreateTaskRequestData;
use TDL\ActorUser\Usecases\CreateTask\ResponseData as CreateTaskResponseData;

use TDL\ActorUser\Usecases\MarkTaskDone\RequestData as MarkTaskDoneRequestData;
use TDL\ActorUser\Usecases\MarkTaskDone\ResponseData as MarkTaskDoneResponseData;

/**
 * Class BoundaryDataFactory
 * @package TDL\ActorUser\Usecases
 * To be used by test api
 */

final class BoundaryDataFactory
{
    /**
     * @param string $useCase
     * @return BoundaryDataInterface
     */
    public function getInstanceRequestData(string $useCase)
    {
        switch ($useCase) {
            case 'CreateTask' :
                return new CreateTaskRequestData();
            case 'MarkTaskDone' :
                return new MarkTaskDoneRequestData();
            default:
                return null;
        }
    }

    /**
     * @param string $useCase
     * @return BoundaryDataInterface
     */
    public function getInstanceResponseData(string $useCase)
    {
        switch ($useCase) {
            case 'CreateTask' :
                return new CreateTaskResponseData();
            case 'MarkTaskDone' :
                return new MarkTaskDoneResponseData();
            default:
                return null;
        }
    }
}