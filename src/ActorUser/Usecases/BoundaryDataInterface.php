<?php
namespace TDL\ActorUser\Usecases;

/**
 * Generic interface for Response and Request data model used in use cases
 * @package TDL\Usecases
 */
interface BoundaryDataInterface {

    public function getDateFields() : array;

    public function getMandatoryFields() : array;

    public function isEmpty() : bool;
}