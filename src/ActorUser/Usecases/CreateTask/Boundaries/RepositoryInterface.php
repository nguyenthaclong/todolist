<?php
namespace TDL\ActorUser\Usecases\CreateTask\Boundaries;

use TDL\Entities\Task\Task;

/**
 * Interface EntityRepositoryInterface
 * @package TDL\Entities
 * Assure inverse dependency abstract link with REPOSITORIES COMPONENT
 */

interface RepositoryInterface {

    /**
     * @param Task $task
     * @return false|Task
     */
    public function persist(Task $task);


}