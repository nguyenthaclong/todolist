<?php
namespace TDL\ActorUser\Usecases\CreateTask;

use TDL\ActorUser\Usecases\BoundaryDataInterface;

final class RequestData implements BoundaryDataInterface {

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $date;

    /**
     * @var int
     */
    public $status;

     /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->id) && empty($this->title) &&
                empty($this->label) && empty($this->date));
    }

    public function getMandatoryFields(): array
    {
        return ['title'];
    }

    public function getDateFields(): array
    {
        return ['date' => 'Y-m-d H:i'];
    }
}