<?php


namespace TDL\ActorUser\Usecases\CreateTask;


use TDL\Entities\EntityFactory;
use TDL\Entities\Task\Task;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RepositoryInterface;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RequesterInterface;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\Exception\TaskRuntimeException;
use TDL\Entities\Task\Exception\TaskNotValidException;


final class CreateTask implements RequesterInterface
{

    /** @var EntityFactory */
    private $entityFactory;

    /** @var RepositoryInterface */
    private $repository;

    /**
     * @param EntityFactory $entityFactory
     * @param RepositoryInterface $repository
     */
    public function __construct(EntityFactory $entityFactory,
                                RepositoryInterface $repository)
    {
        $this->entityFactory = $entityFactory;
        $this->repository    = $repository;
    }

    /**
     * Manage use case create task
     * @param BoundaryDataInterface $requestData
     * @return ResponseData
     * @throws TaskNotValidException
     * @throws TaskRuntimeException
     */
    public function createTask(BoundaryDataInterface $requestData) : BoundaryDataInterface
    {
        # get input data
        /** @var RequestData $input */
        $input = $requestData;

        # use case scenario here
        /** @var Task $task */
        $task = $this->entityFactory->getInstanceTask();
        $taskCreated = $task->create($input->title, $input->label, $input->date);

        # persist data
        /** @var Task $taskPersisted */
        $taskPersisted = $this->repository->persist($taskCreated);
        if (false === $taskPersisted) {
            throw new TaskRuntimeException();
        }

        # prepare response data
        $output = new ResponseData();
        $output->id     = $taskPersisted->getId();
        $output->title  = $taskPersisted->getTitle();
        $output->label  = $taskPersisted->getLabel();
        $output->date   = $taskPersisted->getDate();
        $output->status = $taskPersisted->getStatus();
        return $output;
    }

}