<?php
namespace TDL\Entities;

use TDL\Entities\Task\Task;

class EntityFactory
{
    public function getInstanceTask() : EntityInterface
    {
        return new Task();
    }
}