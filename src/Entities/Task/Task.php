<?php


namespace TDL\Entities\Task;

use TDL\Entities\EntityInterface;
use TDL\Entities\Task\Exception\TaskNotValidException;

class Task implements EntityInterface
{
    const STATUS_UNCOMPLETED = 1;
    const STATUS_COMPLETED   = 2;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $label;

    /**
     * @var int
     */
    private $status;

    /**
     * @var bool
     */
    private $deleted;


    public function __construct()
    {
        $this->deleted = false;
        $this->status  = self::STATUS_UNCOMPLETED;
    }

    /**
     * @param string $id
     * @return Task
     */
    private function setId(string $id): Task
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $title
     * @return Task
     */
    private function setTitle(string $title) : Task
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $date
     * @return Task
     */
    private function setDate(string $date): Task
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @param string $label
     * @return Task
     */
    private function setLabel(string $label): Task
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param int $status
     * @return Task
     */
    private function setStatus(int $status): Task
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    /**
     * @return bool
     */
    public function isUndone(): bool
    {
        return $this->status !== self::STATUS_COMPLETED;
    }



    /**
     * Validate data of entity
     * @param Task $task
     * @return bool
     * @throws TaskNotValidException
     */
    protected function validate(Task $task) : bool
    {
        if (empty($task->title)) {
            throw new TaskNotValidException();
        }

        $pregDate = '/^[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])\s([01][0-9]|2[0-3]):(0[0-9]|[1-5][0-9])/';
        if (!empty($task->date)) {
            if (1 !== preg_match($pregDate, $task->date)) {
                throw new TaskNotValidException();
            }
        }

        if (!empty($task->status) &&
            $task->status !== self::STATUS_UNCOMPLETED &&
            $task->status !== self::STATUS_COMPLETED) {
            throw new TaskNotValidException();
        }

        return true;
    }

    /**
     * Add new task, its a CBD (Critical Business Data)
     * @param string $title
     * @param string $label
     * @param string $date
     * @return Task
     * @throws TaskNotValidException
     */
    public function create(string $title, string $label, string $date)
    {
        $task = new Task();
        $task->setId(uniqid('task', true))
             ->setTitle(trim($title))
             ->setLabel(trim($label))
             ->setDate(trim($date))
             ->setStatus(self::STATUS_UNCOMPLETED);
        $this->validate($task);
        return $task;
    }

    /**
     * Update a task
     * @param string $id
     * @param string $title
     * @param string $label
     * @param string $date
     * @param int $status
     * @return Task
     * @throws TaskNotValidException
     */
    public function update(string $id, string $title, string $label, string $date, int $status) : Task
    {
        $task = new Task();
        $task->setId($id)
             ->setTitle(trim($title))
             ->setLabel(trim($label))
             ->setDate(trim($date))
             ->setStatus($status);
        $this->validate($task);
        return $task;
    }

    /**
     * Mark task completed
     * @return void
     */
    public function markDone()
    {
        $this->status = self::STATUS_COMPLETED;
    }

    /**
     * Mark task uncompleted
     * @return void
     */
    public function markUndone()
    {
        $this->status = self::STATUS_UNCOMPLETED;
    }

    /**
     * Mark task deleted
     * @return void
     */
    public function markDeleted()
    {
        $this->deleted = true;
    }



}