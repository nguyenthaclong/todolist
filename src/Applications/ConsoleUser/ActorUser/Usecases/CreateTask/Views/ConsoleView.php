<?php

namespace TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Views;

use TDL\ActorUser\Usecases\CreateTask\RequestData;
use TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskViewInterface;
use TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskViewData;


class ConsoleView implements CreateTaskViewInterface
{

    public function showCreateTaskResponse(CreateTaskViewData $viewTaskData)
    {
        echo "DONE : Task N°[{$viewTaskData->id}]- [{$viewTaskData->title}] - [{$viewTaskData->label}] has been created.\n";
    }

    public function showCreateTaskInputDataInvalid()
    {
        echo "ERROR : Create task process has failed because bad input data\n";
    }

    public function showCreateTaskRunTimeError()
    {
        echo "ERROR : Create task process has failed because runtime error has raised\n";
    }

    /**
     * @return RequestData
     */
    public function showCreateTaskInputForm() : RequestData
    {
        # ui to get data
        echo "Please enter new task :\n";
        echo "Task title : ";
        $title = trim(fgets(STDIN, 30));
        echo "Task label : ";
        $label = trim(fgets(STDIN, 50));
        echo "Task date (YYYY-mm-dd HH:mm) : ";
        $date = trim(fgets(STDIN, 17));

        # prepare data to return
        $data = new RequestData();
        $data->title = $title;
        $data->label = $label;
        $data->date = $date;

        return $data;

    }
}