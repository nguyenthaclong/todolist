<?php
namespace TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Presenters;

use TDL\ActorUser\Usecases\CreateTask\RequestData;

/**
 * Interface CreateTaskViewInterface
 * Assure inverse dependency abstract link with VIEWS (UI) LAYER
 * @package TDL\Applications\ConsoleUser\Usecases\CreateTask\Presenters;
 */
interface CreateTaskViewInterface {

    public function showCreateTaskInputForm() : RequestData;

    public function showCreateTaskResponse(CreateTaskViewData $viewTaskData);

    public function showCreateTaskInputDataInvalid();

    public function showCreateTaskRunTimeError();
}