<?php

namespace TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Presenters;

use TDL\Applications\ConsoleUser\ActorUser\Controllers\Boundaries\CreateTaskPresenterInterface;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\ResponseData;

class CreateTaskPresenter implements CreateTaskPresenterInterface
{

    /** @var CreateTaskViewInterface */
    private $view;

    public function __construct(CreateTaskViewInterface $view)
    {
        $this->view = $view;
    }

    public function presentCreateTaskInputForm()
    {
        # print out
        $this->view->showCreateTaskInputForm();
    }

    public function presentCreateTaskDone(BoundaryDataInterface $responseData)
    {
        # transform response data structure to a view data structure (in this case console view type)
        /** @var ResponseData $responseData */
        $viewDataStruct = new CreateTaskViewData();
        $viewDataStruct->id      = $responseData->id;
        $viewDataStruct->title   = $responseData->title;
        $viewDataStruct->label   = $responseData->label;
        $viewDataStruct->status  = $responseData->status;
        $viewDataStruct->deleted = $responseData->deleted;

        # print out
        $this->view->showCreateTaskResponse($viewDataStruct);
    }

    public function presentTaskNotValidException()
    {
        # print out
        $this->view->showCreateTaskInputDataInvalid();
    }

    public function presentCreateTaskRunTimeException()
    {
        # print out
        $this->view->showCreateTaskRunTimeError();
    }
}