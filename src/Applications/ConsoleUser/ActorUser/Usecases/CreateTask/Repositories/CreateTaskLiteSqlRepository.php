<?php

namespace TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Repositories;

use PDO;
use PDOException;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RepositoryInterface as CreateTaskRepository;
use TDL\Entities\Task\Exception\TaskNotValidException;
use TDL\Entities\Task\Task;

class CreateTaskLiteSqlRepository implements CreateTaskRepository
{

    /** @var PDO */
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * @param Task $task
     * @return false|Task
     * @throws TaskNotValidException
     */
    public function persist(Task $task): Task
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO TASKS (id, title, label, status, date) VALUES 
                                             (:id, :title, :label, :status, :date)");
            /** @var Task $task */
            $id     = $task->getId();
            $title  = $task->getTitle();
            $label  = $task->getLabel();
            $status = $task->getStatus();
            $date   = $task->getDate();
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->bindParam(':title', $title,PDO::PARAM_STR);
            $stmt->bindParam(':label', $label,PDO::PARAM_STR);
            $stmt->bindParam(':status', $status,PDO::PARAM_INT);
            $stmt->bindParam(':date', $date,PDO::PARAM_STR);
            $stmt->execute();

            # return task created
            $stmt = $this->db->prepare("SELECT * FROM TASKS WHERE id=:id");
            $id = $task->getId();
            $stmt->bindParam(':id',$id ,PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                $taskPersisted = new Task();
                return $taskPersisted->update($row['id'], $row['title'],  $row['label'], $row['date'], $row['status']);
            }

            return false;

        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @param array $query pairs key and value
     * @return false|Task[]
     */
    public function findBy(array $query): array
    {
        // TODO: Implement findBy() method.
    }

    /**
     * @return false|Task[]
     */
    public function listAll(): array
    {
        // TODO: Implement listAll() method.
    }
}