<?php
namespace TDL\Applications\ConsoleUser\ActorUser\Controllers;

use TDL\Entities\Task\Exception\TaskNotValidException;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RequesterInterface as CreateTaskRequester;
use TDL\ActorUser\Usecases\MarkTaskDone\Boundaries\RequesterInterface as MarkTaskDoneRequester;
use TDL\ActorUser\Usecases\CreateTask\Exception\TaskRuntimeException;
use TDL\ActorUser\Usecases\MarkTaskDone\Exception\MarkTaskDoneRuntimeException;

use TDL\Applications\ConsoleUser\ActorUser\Controllers\Boundaries\CreateTaskPresenterInterface;
use TDL\Applications\ConsoleUser\ActorUser\Controllers\Boundaries\MarkTaskDonePresenterInterface;


/**
 * Class UserController
 * @package TDL\ActorUser\Controller
 * Assure inverse dependency abstract link between CORE LAYERS (Core business rules and Entities)
 */
class UserController
{
    /**
     * @param BoundaryDataInterface $requestData
     * @param CreateTaskRequester $requester
     * @param CreateTaskPresenterInterface $presenter
     */
    public function actionCreateTask(BoundaryDataInterface $requestData,
                                     CreateTaskRequester $requester,
                                     CreateTaskPresenterInterface $presenter)
    {
        try {
            $responseData = $requester->createTask($requestData);
            $presenter->presentCreateTaskDone($responseData);
        } catch (TaskNotValidException $e) {
            $presenter->presentTaskNotValidException();
        } catch (TaskRuntimeException $e) {
            $presenter->presentCreateTaskRunTimeException();
        }
    }


    /**
     * @param BoundaryDataInterface $requestData
     * @param MarkTaskDoneRequester $requester
     * @param MarkTaskDonePresenterInterface $presenter
     */
    public function actionMarkTaskDone(BoundaryDataInterface $requestData,
                                       MarkTaskDoneRequester $requester,
                                       MarkTaskDonePresenterInterface $presenter)
    {
        try {
            $responseData = $requester->markTaskDone($requestData);
            $presenter->presentMarkTaskDone($responseData);
        } catch (TaskNotValidException $e) {
                $presenter->presentTaskNotValidException();
        } catch (MarkTaskDoneRuntimeException $e) {
            $presenter->presentMarkTaskDoneRunTimeException();
        }
    }
}
