<?php

namespace TDL\Applications\ConsoleUser\ActorUser\Controllers\Boundaries;

use TDL\ActorUser\Usecases\BoundaryDataInterface;

/**
 * Interface CreateTaskPresenterInterface
 * Assure inverse dependency abstract link with PRESENTERS COMPONENT
 * Specific to use case CreateTask
 * @package TDL\ActorUser\Presenters
 */
interface CreateTaskPresenterInterface
{
    public function presentCreateTaskInputForm();

    public function presentCreateTaskDone(BoundaryDataInterface $responseData);

    public function presentTaskNotValidException();

    public function presentCreateTaskRunTimeException();

}