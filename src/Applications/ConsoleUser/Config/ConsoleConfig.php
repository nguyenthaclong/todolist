<?php
namespace TDL\Applications\ConsoleUser\Config;

use Exception;
use PDO;
use TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Repositories\CreateTaskLiteSqlRepository;
use TDL\Applications\ConsoleUser\Exception\BadSetupException;

final class ConsoleConfig
{
    /** @var array */
    private $config;

    /**
     * ConsoleConfig constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->config = parse_ini_file(__DIR__.'\..\config.ini', true);
        if (false === $this->config) {
            throw new BadSetupException("Config file not found. Exit");
        }
        if (!isset($this->config['database']) || !isset($this->config['fr_FR'])) {
            throw new BadSetupException("Bad config file. Exit");
        }
    }

    /**
     * @return false|CreateTaskLiteSqlRepository
     * @throws Exception
     */
    public function getCreateTaskRepository() : CreateTaskLiteSqlRepository
    {
        if (!isset($this->config['database']['dns']) || empty($this->config['database']['dns'])) {
            throw new BadSetupException("No DNS database found. Exit");
        }
        $db = new PDO($this->config['database']['dns']);
        return new CreateTaskLiteSqlRepository($db);
    }
}