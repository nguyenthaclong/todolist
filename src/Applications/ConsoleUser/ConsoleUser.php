<?php

namespace TDL\Applications\ConsoleUser;

use Exception;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\Entities\EntityFactory;
use TDL\ActorUser\Usecases\CreateTask\RequestData;
use TDL\ActorUser\Usecases\CreateTask\CreateTask;

use TDL\Applications\ConsoleUser\Config\ConsoleConfig;
use TDL\Applications\ConsoleUser\ActorUser\Controllers\UserController;
use TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Views\ConsoleView;
use TDL\Applications\ConsoleUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskPresenter;

/**
 * Main program entry point
 * Class ConsoleUser
 */
class ConsoleUser
{
    protected $registry = [];

    /** @var ConsoleConfig */
    protected $config;

    public function registerCommand($name, $callable)
    {
        $this->registry[$name] = $callable;
    }

    public function getCommand($command)
    {
        return isset($this->registry[$command]) ? $this->registry[$command] : null;
    }

    /**
     * Help route handler
     */
    public function showHelp()
    {
        echo "Usage >php ConsoleUser.php -command ...\n";
        echo "Commandes dispnibles :\n";
        echo "  -help\n";
        echo "  -add \"titre\" \"label\" [\"date\"] \n";
    }

    /**
     * Help route handler
     * @throws Exception
     */
    public function getInputForCreateTask()
    {
        $view = new ConsoleView();
        /** @var BoundaryDataInterface $requestData */
        $requestData = $view->showCreateTaskInputForm();

        $factoryEntity = new EntityFactory();
        $repository    = $this->config->getCreateTaskRepository();
        $useCase       = new CreateTask($factoryEntity, $repository);
        $presenter     = new CreateTaskPresenter($view);
        $controller    = new UserController();
        $controller->actionCreateTask($requestData, $useCase, $presenter);
    }

    /**
     * Add task route handler
     * @param string $title
     * @param string $label
     * @param string $date
     * @throws Exception
     */
    public function addTask(string $title, string $label, string $date = '')
    {
        $repository    = $this->config->getCreateTaskRepository();
        $controller    = new UserController();
        $factoryEntity = new EntityFactory();
        $useCase       = new CreateTask($factoryEntity, $repository);
        $view          = new ConsoleView();
        $presenter     = new CreateTaskPresenter($view);

        $requestData   = new RequestData();
        $requestData->title = $title;
        $requestData->label = $label;
        $requestData->date  = $date;
        $controller->actionCreateTask($requestData, $useCase, $presenter);
    }


    /**
     * Main program
     * @param array $argv
     */
    public function run(array $argv = [])
    {
        # check cli mode
        if (php_sapi_name() !== 'cli') {
            exit;
        }
        try {
            # read config file
            $this->config = new ConsoleConfig();

            # manage command
            $command_name = "help";
            if (isset($argv[1])) {
                $command_name = $argv[1];
            }
            $command = $this->getCommand($command_name);
            if ($command === null) {
                echo "ERROR: Command $command_name not found.\n";
                $this->showHelp();
                exit;
            }

            # call handlers
            call_user_func($command, $argv);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }
}
require __DIR__ . '/../../../vendor/autoload.php';
$app = new ConsoleUser();

$app->registerCommand('-new', function (array $argv) use ($app) {
    $app->getInputForCreateTask();
});

$app->registerCommand('-add', function (array $argv) use ($app) {
    if (!isset($argv[2]) || !isset($argv[3])) {
        $app->showHelp();
        exit();
    }
    $app->addTask($argv[2], $argv[3], (!isset($argv[4]) ? '' : $argv[4]));
});

$app->registerCommand('-help', function (array $argv) use ($app) {
    $app->showHelp();
});

$app->run($argv);