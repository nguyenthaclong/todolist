<?php

namespace TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Views;

use Psr\Http\Message\ResponseInterface;
use Slim\Views\PhpRenderer;
use TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskViewInterface;
use TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskViewData;

class CreateTaskView implements CreateTaskViewInterface
{

    /**
     * @var PhpRenderer
     */
    private $renderer;

    public function __construct(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }
    
    public function showCreateTaskResponse(ResponseInterface $response, CreateTaskViewData $viewTaskData) : ResponseInterface
    {
        return $this->renderer->render($response, 'Usecases/CreateTask/TaskAdded.phtml', ['viewData' => $viewTaskData]);
    }

    public function showCreateTaskInputDataInvalid(ResponseInterface $response) : ResponseInterface
    {
        return $this->renderer->render($response, 'Usecases/CreateTask/NewTask.phtml', ['viewData' => ['error' => 'InputDataInvalid']]);
    }

    public function showCreateTaskRunTimeError(ResponseInterface $response) : ResponseInterface
    {
        return $this->renderer->render($response, 'Usecases/CreateTask/NewTask.phtml',  ['viewData' => ['error' => 'RunTimeError']]);
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function showCreateTaskInputForm(ResponseInterface $response) : ResponseInterface
    {
        return $this->renderer->render($response, 'Usecases/CreateTask/NewTask.phtml', []);
    }
}