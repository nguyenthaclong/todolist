<?php
namespace TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters;

use Psr\Http\Message\ResponseInterface;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\ResponseData;
use TDL\Applications\SlimUser\ActorUser\Controllers\Boundaries\CreateTaskPresenterInterface;

class CreateTaskPresenter implements CreateTaskPresenterInterface
{

    /** @var CreateTaskViewInterface */
    private $view;

    /**
     * @var ResponseInterface
     */
    private $response;

    public function __construct(CreateTaskViewInterface $view, ResponseInterface $response)
    {
        $this->view = $view;
        $this->response = $response;
    }

    /**
     * @return ResponseInterface
     */
    public function presentCreateTaskInputForm() : ResponseInterface
    {
        # print out
        return $this->view->showCreateTaskInputForm($this->response);
    }

    /**
     * @param BoundaryDataInterface $responseData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function presentCreateTaskDone(BoundaryDataInterface $responseData) : ResponseInterface
    {
        # transform response data structure to a view data structure (in this case console view type)
        /** @var ResponseData $responseData */
        $viewDataStruct = new CreateTaskViewData();
        $viewDataStruct->id      = $responseData->id;
        $viewDataStruct->title   = htmlspecialchars($responseData->title);
        $viewDataStruct->label   = htmlspecialchars($responseData->label);
        $viewDataStruct->date    = date_format(new \DateTime($responseData->date), 'd/m/Y H:i');
        $viewDataStruct->status  = $responseData->status;
        $viewDataStruct->deleted = $responseData->deleted;

        # print out
        return $this->view->showCreateTaskResponse($this->response, $viewDataStruct);
    }

    /**
     * @return ResponseInterface
     */
    public function presentTaskNotValidException() : ResponseInterface
    {
        # print out
        return $this->view->showCreateTaskInputDataInvalid($this->response);
    }

    /**
     * @return ResponseInterface
     */
    public function presentCreateTaskRunTimeException() : ResponseInterface
    {
        # print out
        return $this->view->showCreateTaskRunTimeError($this->response);
    }
}