<?php
namespace TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface CreateTaskViewInterface
 * Assure inverse dependency abstract link with VIEWS (UI) LAYER
 * @package TDL\Applications\ConsoleUser\Usecases\CreateTask\Presenters;
 */
interface CreateTaskViewInterface {

    public function showCreateTaskInputForm(ResponseInterface $response) : ResponseInterface;

    public function showCreateTaskResponse(ResponseInterface $response, CreateTaskViewData $viewTaskData): ResponseInterface;

    public function showCreateTaskInputDataInvalid(ResponseInterface $response) : ResponseInterface;

    public function showCreateTaskRunTimeError(ResponseInterface $response) : ResponseInterface;
}