<?php

namespace TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters;

class CreateTaskViewData
{
    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $date;

    /** @var string */
    public $label;

    /** @var string */
    public $status;

    /** @var bool */
    public $deleted;

}