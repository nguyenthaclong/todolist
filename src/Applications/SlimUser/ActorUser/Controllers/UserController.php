<?php
namespace TDL\Applications\SlimUser\ActorUser\Controllers;

use Psr\Http\Message\ResponseInterface;

use TDL\Entities\Task\Exception\TaskNotValidException;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RequesterInterface as CreateTaskRequester;
use TDL\ActorUser\Usecases\MarkTaskDone\Boundaries\RequesterInterface as MarkTaskDoneRequester;
use TDL\ActorUser\Usecases\CreateTask\Exception\TaskRuntimeException;
use TDL\ActorUser\Usecases\MarkTaskDone\Exception\MarkTaskDoneRuntimeException;

use TDL\Applications\SlimUser\ActorUser\Controllers\Boundaries\CreateTaskPresenterInterface;
use TDL\Applications\SlimUser\ActorUser\Controllers\Boundaries\MarkTaskDonePresenterInterface;


/**
 * Class UserController
 * @package TDL\ActorUser\Controller
 * Assure inverse dependency abstract link between CORE LAYERS (Core business rules and Entities)
 */
class UserController
{
    /**
     * @param BoundaryDataInterface $requestData
     * @param CreateTaskRequester $requester
     * @param CreateTaskPresenterInterface $presenter
     * @return ResponseInterface
     */
    public function actionCreateTask(BoundaryDataInterface $requestData,
                                     CreateTaskRequester $requester,
                                     CreateTaskPresenterInterface $presenter) : ResponseInterface
    {
        try {
            $responseData = $requester->createTask($requestData);
            return $presenter->presentCreateTaskDone($responseData);
        } catch (TaskNotValidException $e) {
            return $presenter->presentTaskNotValidException();
        } catch (TaskRuntimeException $e) {
            return $presenter->presentCreateTaskRunTimeException();
        }
    }


    /**
     * @param BoundaryDataInterface $requestData
     * @param MarkTaskDoneRequester $requester
     * @param MarkTaskDonePresenterInterface $presenter
     */
    public function actionMarkTaskDone(BoundaryDataInterface $requestData,
                                       MarkTaskDoneRequester $requester,
                                       MarkTaskDonePresenterInterface $presenter)
    {
        try {
            $responseData = $requester->markTaskDone($requestData);
            return $presenter->presentMarkTaskDone($responseData);
        } catch (TaskNotValidException $e) {
            return $presenter->presentTaskNotValidException();
        } catch (MarkTaskDoneRuntimeException $e) {
            return $presenter->presentMarkTaskDoneRunTimeException();
        }
    }
}
