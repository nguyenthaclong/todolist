<?php

namespace TDL\Applications\SlimUser\ActorUser\Controllers\Boundaries;

use TDL\ActorUser\Usecases\BoundaryDataInterface;

/**
 * Interface MarkTaskDonePresenterInterface
 * Assure inverse dependency abstract link with PRESENTERS COMPONENT
 * Specific to use case CreateTask
 * @package TDL\ActorUser\Presenters
 */
interface MarkTaskDonePresenterInterface
{
    public function presentMarkTaskDoneInputForm();

    public function presentMarkTaskDone(BoundaryDataInterface $responseData);

    public function presentTaskNotValidException();

    public function presentMarkTaskDoneRunTimeException();

}