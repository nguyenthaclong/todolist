<?php

namespace TDL\Applications\SlimUser\ActorUser\Controllers\Boundaries;

use Psr\Http\Message\ResponseInterface;
use TDL\ActorUser\Usecases\BoundaryDataInterface;

/**
 * Interface CreateTaskPresenterInterface
 * Assure inverse dependency abstract link with PRESENTERS COMPONENT
 * Specific to use case CreateTask
 * @package TDL\ActorUser\Presenters
 */
interface CreateTaskPresenterInterface
{
    public function presentCreateTaskInputForm() : ResponseInterface;

    public function presentCreateTaskDone(BoundaryDataInterface $responseData) : ResponseInterface;

    public function presentTaskNotValidException() : ResponseInterface;

    public function presentCreateTaskRunTimeException() : ResponseInterface;

}