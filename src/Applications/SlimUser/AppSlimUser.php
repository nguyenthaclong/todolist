<?php
namespace TDL\Applications\SlimUser;

use Psr\Http\Message\ResponseInterface;
use TDL\Entities\EntityFactory;
use TDL\Applications\SlimUser\ActorUser\Controllers\UserController;
use TDL\ActorUser\Usecases\BoundaryDataInterface;
use TDL\ActorUser\Usecases\CreateTask\CreateTask;
use TDL\ActorUser\Usecases\CreateTask\RequestData;

use TDL\Applications\SlimUser\Config\SlimConfig;
use TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Repositories\CreateTaskLiteSqlRepository;
use TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Presenters\CreateTaskPresenter;
use TDL\Applications\SlimUser\ActorUser\Usecases\CreateTask\Views\CreateTaskView;

use Slim\App;

class AppSlimUser
{

    /**
     * @var App
     */
    private $app;

    /**
     * @var SlimConfig
     */
    private $config;

    /**
     * AppSlimUser constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->config = new SlimConfig();
        $this->app    = new App(['settings' => $this->config->getSetupParameters()]);
    }

    /**
     * Return slim app
     * @return App
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * GET
     * /task/new
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function handleGetCreateTask(ResponseInterface $response) : ResponseInterface
    {
        $view = new CreateTaskView($this->config->getRenderer());
        return $view->showCreateTaskInputForm($response);
    }

    /**
     * POST
     * /task/add
     * @param array $requestRawData
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handlePostCreateTask(array $requestRawData, ResponseInterface $response) : ResponseInterface
    {
        /** @var BoundaryDataInterface $requestData */
        $requestData = new \TDL\ActorUser\Usecases\CreateTask\RequestData();
        $requestData->title = filter_var($requestRawData['title'], FILTER_SANITIZE_STRING);
        $requestData->label = filter_var($requestRawData['label'], FILTER_SANITIZE_STRING);
        $requestData->date  = filter_var($requestRawData['date'], FILTER_SANITIZE_STRING);


        $factoryEntity = new EntityFactory();
        $repository    = new CreateTaskLiteSqlRepository($this->config->getRepository());
        $useCase       = new CreateTask($factoryEntity, $repository);
        $view          = new CreateTaskView($this->config->getRenderer());
        $presenter     = new CreateTaskPresenter($view, $response);
        $controller    = new UserController();
        return $controller->actionCreateTask($requestData, $useCase, $presenter);
    }
}