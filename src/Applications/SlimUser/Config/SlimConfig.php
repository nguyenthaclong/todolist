<?php
namespace TDL\Applications\SlimUser\Config;

use PDO;
use Slim\Views\PhpRenderer;
use TDL\Applications\SlimUser\Exception\BadSetupException;

final class SlimConfig
{
    /** @var array */
    private $config = [];

    /**
     * ConsoleConfig constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        # get configuration parameters
        $this->config = parse_ini_file(__DIR__.'\..\config.ini', true);
        if (false === $this->config) {
            throw new BadSetupException("Config file not found. Exit");
        }
        if (!isset($this->config['database']) || !isset($this->config['fr_FR'])) {
            throw new BadSetupException("Bad config file. Exit");
        }

    }

    /**
     * Return setting in config.ini as array
     * @return array
     */
    public function getSetupParameters()
    {
        return $this->config;
    }

    /**
     * @return PhpRenderer
     */
    public function getRenderer()
    {
        return new PhpRenderer(__DIR__.'\..\Templates\\');
    }

    /**
     * @return PDO
     * @throws \Exception
     */
    public function getRepository() : PDO
    {
        if (!isset($this->config['database']['dns']) || empty($this->config['database']['dns'])) {
            throw new BadSetupException("No DNS database found. Exit");
        }

        return new PDO($this->config['database']['dns']);
    }
}