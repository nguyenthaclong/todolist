<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use TDL\Applications\SlimUser\AppSlimUser;


require '../../../../vendor/autoload.php';

$appSlimUser = new AppSlimUser();

$appSlimUser->getApp()->get('/task/new', function (Request $request, Response $response, array $args) use ($appSlimUser) {
    return $appSlimUser->handleGetCreateTask($response);
});

$appSlimUser->getApp()->post('/task/new', function (Request $request, Response $response, array $args) use ($appSlimUser) {
    $data = $request->getParsedBody();
    return $appSlimUser->handlePostCreateTask($data, $response);
});

$appSlimUser->getApp()->run();