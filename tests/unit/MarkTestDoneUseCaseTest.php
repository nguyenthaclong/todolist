<?php

namespace Tests\unit;

use Codeception\Test\Unit;

use TDL\ActorUser\Usecases\BoundaryDataFactory;
use TDL\ActorUser\Usecases\MarkTaskDone\MarkTaskDone;
use TDL\ActorUser\Usecases\MarkTaskDone\Boundaries\RepositoryInterface;
use TDL\ActorUser\Usecases\MarkTaskDone\Exception\MarkTaskDoneRuntimeException;
use TDL\ActorUser\Usecases\MarkTaskDone\ResponseData;
use TDL\ActorUser\Usecases\MarkTaskDone\RequestData;

use TDL\Entities\Task\Task;
use TDL\Entities\EntityFactory;
use TDL\Entities\Task\Exception\TaskNotValidException;

use TDL\TestApi\DataModelHelper;

class MarkTaskDoneUseCaseTest extends Unit
{
    /**
     * @var MarkTaskDone
     */
    protected $useCase;

    /**
     * @var RequestData
     */
    protected $requestData;

    /**
     * @var ResponseData
     */
    protected $responseData;

    /** @var  RepositoryInterface */
    protected $repository;

    /** @var EntityFactory */
    protected $factory;

    /**
     * @var array
     */
    protected $requestDataStruct;

    /**
     * @var DataModelHelper
     */
    protected $dataStructHelper;

    /**
     * @var array
     */
    protected $responseDataStruct;

    public function setUp()
    {
        # set up objects

        /** @var EntityFactory $factory */
        $this->factory    = new EntityFactory();
        $this->repository = $this->getMockForAbstractClass(RepositoryInterface::class);

        # setup data structure
        $this->dataStructHelper = new DataModelHelper(new BoundaryDataFactory(), 'MarkTaskDone');
    }

    /**
     * Test a regular scenario where create task will succeed
     * @throws TaskNotValidException
     * @throws MarkTaskDoneRuntimeException
     */
    public function testMarkTaskDoneRegular()
    {
        #prepare request data
        $request = $this->dataStructHelper->createFakeRequestData();

        # prepare response data
        /** @var Task $task */
        $task = $this->factory->getInstanceTask();
        $taskReturned = $task->update($request->id, $request->title, $request->label, $request->date, $task::STATUS_COMPLETED);
        $this->repository->expects($this->exactly(1))
                         ->method('persistTaskDone')
                         ->will($this->returnValue($taskReturned));

        # invoke use case
        $this->useCase      = new MarkTaskDone($this->factory, $this->repository);
        $response = $this->useCase->markTaskDone($request);

        #assert response
        $this->assertEquals($task::STATUS_COMPLETED, $response->status, 'Check status is done');
        $this->assertEquals($request->id, $response->id, 'Same id');
    }


    public function testMarkTaskDoneWithPersistIssue()
    {
        #assert TaskNotValidException
        $this->expectException(MarkTaskDoneRuntimeException::class);

        #prepare request data
        $request = $this->dataStructHelper->createFakeRequestData();

        # prepare response data
        /** @var Task $task */
        $task = $this->factory->getInstanceTask();
        $this->repository->expects($this->exactly(1))
                         ->method('persistTaskDone')
                         ->will($this->returnValue(false));

        # invoke use case
        $this->useCase      = new MarkTaskDone($this->factory, $this->repository);
        $response = $this->useCase->markTaskDone($request);

    }

    public function testMarkTaskDoneWithRequestDataInvalid()
    {
        #assert TaskNotValidException
        $this->expectException(TaskNotValidException::class);

        #prepare request data
        $request = $this->dataStructHelper->createFakeRequestData();
        $request->title = ' ';

        # prepare response data
        /** @var Task $task */
        $task = $this->factory->getInstanceTask();
        $this->repository->expects($this->any())
            ->method('persistTaskDone');

        # invoke use case
        $this->useCase      = new MarkTaskDone($this->factory, $this->repository);
        $response = $this->useCase->markTaskDone($request);

    }

}