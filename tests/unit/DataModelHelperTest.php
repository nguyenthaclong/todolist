<?php

namespace Tests\unit;

use Codeception\Test\Unit;
use TDL\ActorUser\Usecases\BoundaryDataFactory;
use TDL\ActorUser\Usecases\CreateTask\RequestData;
use TDL\TestApi\DataModelHelper;

class DataModelHelperTest extends Unit
{
    public function testGetPropertiesBoundaryData()
    {
        $dataModelHelper = new DataModelHelper(new BoundaryDataFactory());
        $fakeRequestData = $dataModelHelper->createFakeRequestData();
        $this->assertTrue($fakeRequestData instanceof RequestData);
    }
}