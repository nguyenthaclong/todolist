<?php

namespace Tests\unit;

use Codeception\Test\Unit;

use TDL\ActorUser\Usecases\BoundaryDataFactory;
use TDL\ActorUser\Usecases\CreateTask\CreateTask;
use TDL\ActorUser\Usecases\CreateTask\Boundaries\RepositoryInterface;
use TDL\ActorUser\Usecases\CreateTask\Exception\TaskRuntimeException;
use TDL\ActorUser\Usecases\CreateTask\ResponseData;
use TDL\ActorUser\Usecases\CreateTask\RequestData;

use TDL\Entities\Task\Task;
use TDL\Entities\EntityFactory;
use TDL\Entities\Task\Exception\TaskNotValidException;

use TDL\TestApi\DataModelHelper;


class CreateTaskUseCaseTest extends Unit
{
    /**
     * @var CreateTask
     */
    protected $useCase;

    /**
     * @var RequestData
     */
    protected $requestData;

    /**
     * @var ResponseData
     */
    protected $responseData;

    /** @var  RepositoryInterface */
    protected $repository;

    /** @var EntityFactory */
    protected $factory;

    /**
     * @var array
     */
    protected $requestDataStruct;

    /**
     * @var DataModelHelper
     */
    protected $dataStructHelper;

    /**
     * @var array
     */
    protected $responseDataStruct;

    public function setUp()
    {
        # set up objects

        /** @var EntityFactory $factory */
        $this->factory    = new EntityFactory();
        $this->repository = $this->getMockForAbstractClass(RepositoryInterface::class);

        # setup data structure
        $this->dataStructHelper = new DataModelHelper(new BoundaryDataFactory(), 'CreateTask');
    }

    /**
     * Test a regular scenario where create task will succeed
     */
    public function testCreateNewTaskRegular()
    {
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();

        # prepare response data
        /** @var Task $taskReturned */
        $task = $this->factory->getInstanceTask();
        $taskReturned = $task->create($request->title, $request->label, $request->date);

        $this->repository->expects($this->exactly(1))
                         ->method('persist')
                         ->will($this->returnValue($taskReturned));

        # invoke use case
        $this->useCase      = new CreateTask($this->factory, $this->repository);
        $response = $this->useCase->createTask($request);

        #assert response
        $assertable = $response->getMandatoryFields();
        foreach ($assertable as $field) {
            $this->assertEquals($response->$field, $request->$field);
        }
    }

    public function testCreateTaskWithInvalidDate()
    {
        #assert TaskNotValidException
        $this->expectException(TaskNotValidException::class);

        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();
        $request->date = '11/10/1976'; //wrong format date

        # prepare response data
        $this->repository->expects($this->any())
                         ->method('persist');

        # invoke use case
        $this->useCase      = new CreateTask($this->factory, $this->repository);
        $response = $this->useCase->createTask($request);

    }


    public function testPersistFailure()
    {
        #assert TaskRuntimeException
        $this->expectException(TaskRuntimeException::class);

        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();

        # prepare response data
        $this->repository->expects($this->once())
                         ->method('persist')
                         ->will($this->returnValue(false));

        # invoke use case
        $this->useCase      = new CreateTask($this->factory, $this->repository);
        $response = $this->useCase->createTask($request);
    }


    public function testCreateNewTaskWithEmptyTitle()
    {
        #assert TaskRuntimeException
        $this->expectException(TaskNotValidException::class);

        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();
        $request->title = '';

        # prepare response data
        $this->repository->expects($this->any())
                          ->method('persist');
        # invoke use case
        $this->useCase      = new CreateTask($this->factory, $this->repository);
        $response = $this->useCase->createTask($request);
    }

    public function testCreateNewTaskWithEmptyTitleWithSpaces()
    {
        #assert TaskRuntimeException
        $this->expectException(TaskNotValidException::class);

        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();
        $request->title = '    ';

        # prepare response data
        $this->repository->expects($this->any())
                         ->method('persist');

        # invoke use case
        $this->useCase      = new CreateTask($this->factory, $this->repository);
        $response = $this->useCase->createTask($request);
    }
}
