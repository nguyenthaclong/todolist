<?php
namespace Tests\helper;

use Faker\Factory;

require_once 'vendor/autoload.php';

class DataStructureHelper
{
    /**
     * Generator for boundaries data
     * @param array $input
     * @return array
     */
    public function createFakeDataFromStruct(array $input) : array
    {
        $output = [];
        $faker = Factory::create();
        foreach ($input as $id => $prop) {
            switch ($prop['type']) {
                case 'bool':
                    $output[$id] = $faker->boolean;
                    break;
                case 'date' :
                    $output[$id] = $faker->date('Y-m-d H:i');
                    break;
                case 'string':
                default:
                    $output[$id] = $faker->name;
                    break;
            }
        }
        return $output;
    }

    /**
     * Get mandatory field name using in asserting action
     * @param array $input
     * @return array
     */
    public function getMandatoryItemData(array $input)
    {
        $mandatory = [];
        foreach ($input as $id => $prop) {
            if (false === $prop['optional']) {
                $mandatory[] = $id;
            }
        }
        return $mandatory;
    }
}